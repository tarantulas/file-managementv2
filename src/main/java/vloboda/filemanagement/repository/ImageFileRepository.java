package vloboda.filemanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vloboda.filemanagement.model.ImageFile;

@Repository
public interface ImageFileRepository extends JpaRepository<ImageFile, Long> {

        }
