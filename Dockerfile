
FROM amazoncorretto:17-alpine3.17
EXPOSE 8080
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} file-management.jar
CMD ["java", "-jar", "/file-management.jar"]